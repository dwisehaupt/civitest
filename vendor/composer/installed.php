<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'drupal/recommended-project',
  ),
  'versions' => 
  array (
    'adrienrn/php-mimetyper' => 
    array (
      'pretty_version' => '0.2.2',
      'version' => '0.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '702e00a604b4baed34d69730ce055e05c0f43932',
    ),
    'asm89/stack-cors' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9c31def6a83f84b4d4a40d35996d375755f0e08',
    ),
    'brick/math' => 
    array (
      'pretty_version' => '0.10.2',
      'version' => '0.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '459f2781e1a08d52ee56b0b1444086e038561e3f',
    ),
    'brick/money' => 
    array (
      'pretty_version' => '0.7.1',
      'version' => '0.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '933b375c4cd31c9b5c34b2c8eaaafa2d43ea8f45',
    ),
    'chi-teck/drupal-code-generator' => 
    array (
      'pretty_version' => '2.6.2',
      'version' => '2.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '22ed1cc02dc47814e8239de577da541e9b9bd980',
    ),
    'civicrm/civicrm-asset-plugin' => 
    array (
      'pretty_version' => 'v1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f5a4193997ae4050df515c09e2741eff85b4de40',
    ),
    'civicrm/civicrm-core' => 
    array (
      'pretty_version' => '5.64.4',
      'version' => '5.64.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eb19c3403f3dd644303043b309620495efa7f3ba',
    ),
    'civicrm/civicrm-cxn-rpc' => 
    array (
      'pretty_version' => 'v0.20.12.02',
      'version' => '0.20.12.02',
      'aliases' => 
      array (
      ),
      'reference' => '72ae98b79f04048ed03c4325b5fa4ad501bcec59',
    ),
    'civicrm/civicrm-drupal-8' => 
    array (
      'pretty_version' => '5.64.4',
      'version' => '5.64.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '96f441ed7b91a35f95fa019c37d1524d633e54ed',
    ),
    'civicrm/civicrm-packages' => 
    array (
      'pretty_version' => '5.64.4',
      'version' => '5.64.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b3e01a8c6f2cdd5964e289d443118eaf6cca46b8',
    ),
    'civicrm/composer-compile-lib' => 
    array (
      'pretty_version' => 'v0.7',
      'version' => '0.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '658230901ee3fc2830e9f93239a635a086b6816d',
    ),
    'civicrm/composer-compile-plugin' => 
    array (
      'pretty_version' => 'v0.20',
      'version' => '0.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0933ff53cd720ddcb24bdc5984879673ea1e0cbe',
    ),
    'civicrm/composer-downloads-plugin' => 
    array (
      'pretty_version' => 'v3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3aabb6d259a86158d01829fc2c62a2afb9618877',
    ),
    'civicrm/cv' => 
    array (
      'pretty_version' => 'v0.3.48',
      'version' => '0.3.48.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af01e9c33697af5c01858e7db5688c538f624a6b',
    ),
    'civicrm/cv-lib' => 
    array (
      'replaced' => 
      array (
        0 => 'v0.3.48',
      ),
    ),
    'composer/installers' => 
    array (
      'pretty_version' => 'v1.12.0',
      'version' => '1.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd20a64ed3c94748397ff5973488761b22f6d3f19',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '3.3.2',
      'version' => '3.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3953f23262f2bff1919fc82183ad9acb13ff62c9',
    ),
    'consolidation/annotated-command' => 
    array (
      'pretty_version' => '4.9.1',
      'version' => '4.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e01152f698eff4cb5df3ebfe5e097ef335dbd3c9',
    ),
    'consolidation/config' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '597f8d7fbeef801736250ec10c3e190569b1b0ae',
    ),
    'consolidation/filter-via-dot-access-data' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb2eeba41f8e2e3c61698a5cf70ef048ff6c9d5b',
    ),
    'consolidation/log' => 
    array (
      'pretty_version' => '2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3ad08dc57e8aff9400111bad36beb0ed387fe6a9',
    ),
    'consolidation/output-formatters' => 
    array (
      'pretty_version' => '4.3.2',
      'version' => '4.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '06711568b4cd169700ff7e8075db0a9a341ceb58',
    ),
    'consolidation/robo' => 
    array (
      'pretty_version' => '4.0.2',
      'version' => '4.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ccf80963abf11bdb8e90659aa99a7449b21e9452',
    ),
    'consolidation/self-update' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '972a1016761c9b63314e040836a12795dff6953a',
    ),
    'consolidation/site-alias' => 
    array (
      'pretty_version' => '3.1.7',
      'version' => '3.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b6519592c7e8557423f935806cd73adf69ed6c7',
    ),
    'consolidation/site-process' => 
    array (
      'pretty_version' => '4.2.1',
      'version' => '4.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ee3bf69001694b2117cc2f96c2ef70d8d45f1234',
    ),
    'container-interop/container-interop' => 
    array (
      'replaced' => 
      array (
        0 => '^1.2.0',
      ),
    ),
    'cweagans/composer-patches' => 
    array (
      'pretty_version' => '1.7.3',
      'version' => '1.7.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e190d4466fe2b103a55467dfa83fc2fecfcaf2db',
    ),
    'dflydev/apache-mime-types' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f30a57e59b7476e4c5270b6a0727d79c9c0eb861',
    ),
    'dflydev/dot-access-data' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f41715465d65213d644d3141a6a93081be5d3549',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.13.3',
      'version' => '1.13.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '648b0343343565c4a056bfc8392201385e8d89f0',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c268e882d4dbdd85e36e4ad69e02dc284f89d229',
    ),
    'doctrine/reflection' => 
    array (
      'pretty_version' => '1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '6bcea3e81ab8b3d0abe5fde5300bbc8a968960c7',
    ),
    'dompdf/dompdf' => 
    array (
      'pretty_version' => 'v2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8d2d5e37e8b0b30f0732a011295ab80680d7e85',
    ),
    'drupal/core' => 
    array (
      'pretty_version' => '9.5.11',
      'version' => '9.5.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '8afcb233c2a71501b35fed2713167c37831d5c19',
    ),
    'drupal/core-annotation' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-assertion' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-bridge' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-class-finder' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-composer-scaffold' => 
    array (
      'pretty_version' => '9.5.11',
      'version' => '9.5.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '08da8e59c6f1bd0b1a58d18f8addc0d937bbacc7',
    ),
    'drupal/core-datetime' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-dependency-injection' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-diff' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-discovery' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-event-dispatcher' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-file-cache' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-file-security' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-filesystem' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-front-matter' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-gettext' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-graph' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-http-foundation' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-php-storage' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-plugin' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-project-message' => 
    array (
      'pretty_version' => '9.5.11',
      'version' => '9.5.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '5dfa0b75a057caf6542be67f61e7531c737db48c',
    ),
    'drupal/core-proxy-builder' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-recommended' => 
    array (
      'pretty_version' => '9.5.11',
      'version' => '9.5.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af3521be5376e333ddcdbd31c5a169f16423b46f',
    ),
    'drupal/core-render' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-serialization' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-transliteration' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-utility' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-uuid' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/core-version' => 
    array (
      'replaced' => 
      array (
        0 => '9.5.11',
      ),
    ),
    'drupal/recommended-project' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'drush/drush' => 
    array (
      'pretty_version' => '11.6.0',
      'version' => '11.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f301df5dec8d2aacb03d3e01e0ffc6d98e10ae78',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.2.6',
      'version' => '3.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5997fa97e8790cdae03a9cbd5e78e45e3c7bda7',
    ),
    'enlightn/security-checker' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '196bacc76e7a72a63d0e1220926dbb190272db97',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.16.0',
      'version' => '4.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '523407fb06eb9e5f3d59889b3978d5bfe94299c8',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v5.5.1',
      'version' => '5.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '83b609028194aa042ea33b5af2d41a7427de80e6',
    ),
    'grasmash/expander' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7cbc1f2fdf9a9c0e253a424c2a4058316b7cb6e',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.8',
      'version' => '6.5.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a52f0440530b54fa079ce76e8c5d196a42cad981',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.5.3',
      'version' => '1.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '67ab6e18aaa14d753cc148911d273f6e6cb6721e',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e4490cabc77465aaee90b20cfc9a770f8c04be6b',
    ),
    'henrikbjorn/lurker' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'html2text/html2text' => 
    array (
      'pretty_version' => '4.3.1',
      'version' => '4.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '61ad68e934066a6f8df29a3d23a6460536d0855c',
    ),
    'laminas/laminas-diactoros' => 
    array (
      'replaced' => 
      array (
        0 => '2.18.1',
      ),
    ),
    'laminas/laminas-escaper' => 
    array (
      'pretty_version' => '2.12.0',
      'version' => '2.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ee7a4c37bf3d0e8c03635d5bddb5bb3184ead490',
    ),
    'laminas/laminas-feed' => 
    array (
      'pretty_version' => '2.18.2',
      'version' => '2.18.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a57fdb9df42950d5b7f052509fbdab0d081c6b6d',
    ),
    'laminas/laminas-servicemanager' => 
    array (
      'pretty_version' => '3.17.0',
      'version' => '3.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '360be5f16955dd1edbcce1cfaa98ed82a17f02ec',
    ),
    'laminas/laminas-stdlib' => 
    array (
      'pretty_version' => '3.13.0',
      'version' => '3.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '66a6d03c381f6c9f1dd988bf8244f9afb9380d76',
    ),
    'league/container' => 
    array (
      'pretty_version' => '4.2.0',
      'version' => '4.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '375d13cb828649599ef5d48a339c4af7a26cd0ab',
    ),
    'league/csv' => 
    array (
      'pretty_version' => '9.7.4',
      'version' => '9.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '002f55f649e7511710dc7154ff44c7be32c8195c',
    ),
    'league/oauth2-client' => 
    array (
      'pretty_version' => '2.7.0',
      'version' => '2.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '160d6274b03562ebeb55ed18399281d8118b76c8',
    ),
    'league/oauth2-google' => 
    array (
      'pretty_version' => '3.0.4',
      'version' => '3.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '6b79441f244040760bed5fdcd092a2bda7cf34c6',
    ),
    'lesser-evil/shell-verbosity-is-evil' => 
    array (
      'pretty_version' => 'v1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4195bce7e3adaeda6d4747a1f3ec3c39b124f169',
    ),
    'longwave/laminas-diactoros' => 
    array (
      'pretty_version' => '2.14.2',
      'version' => '2.14.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae4f0becf249ae8eea8f2f8f9fb927104e55a885',
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.2.6',
      'version' => '2.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '30ad6f93cf3efe4192bc7a4c9cad11ff8f4f237f',
    ),
    'marcj/topsort' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '387086c2db60ee0a27ac5df588c0f0b30c6bdc4b',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c56caa1cf5c766ad6d65b6344b807c1e8405b9',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '728434227fe21be27ff6d86621a1b13107a2562c',
    ),
    'masterminds/html5' => 
    array (
      'pretty_version' => '2.7.6',
      'version' => '2.7.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '897eb517a343a2281f11bc5556d6548db7d93947',
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.8.4',
      'version' => '1.8.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a867478eae49c9f59ece437ae7f9506bfaa27483',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.17.1',
      'version' => '4.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a6303e50c90c355c7eeee2c4a8b27fe8dc8fef1d',
    ),
    'orno/di' => 
    array (
      'replaced' => 
      array (
        0 => '~2.0',
      ),
    ),
    'padaliyajay/php-autoprefixer' => 
    array (
      'pretty_version' => '1.4',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '17ac9d20c0e4521163a061d4f99f3d31f88e797e',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'pear/archive_tar' => 
    array (
      'pretty_version' => '1.4.14',
      'version' => '1.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d761c5334c790e45ef3245f0864b8955c562caa',
    ),
    'pear/auth_sasl' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db1ead3dc0bf986d2bab0dbc04d114800cf91dee',
    ),
    'pear/console_getopt' => 
    array (
      'pretty_version' => 'v1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a41f8d3e668987609178c7c4a9fe48fecac53fa0',
    ),
    'pear/db' => 
    array (
      'pretty_version' => 'v1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e4f33dcecd99595df982ef8f56c1d7c2bbeaa21',
    ),
    'pear/log' => 
    array (
      'pretty_version' => '1.13.3',
      'version' => '1.13.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '21af0be11669194d72d88b5ee9d5f176dc75d9a3',
    ),
    'pear/mail' => 
    array (
      'pretty_version' => 'v1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1665e1b1dc0261a15cb3ee99ef38b4254aa7564a',
    ),
    'pear/mail_mime' => 
    array (
      'pretty_version' => '1.10.11',
      'version' => '1.10.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd4fb9ce61201593d0f8c6db629c45e29c3409c14',
    ),
    'pear/net_smtp' => 
    array (
      'pretty_version' => '1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cfd963dc5cc73b4d64c7769e47dfa0f439dec536',
    ),
    'pear/net_socket' => 
    array (
      'pretty_version' => '1.0.14',
      'version' => '1.0.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fcd33efd77e4b35ce85489141ab9145343579fe8',
    ),
    'pear/pear-core-minimal' => 
    array (
      'pretty_version' => 'v1.10.13',
      'version' => '1.10.13.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aed862e95fd286c53cc546734868dc38ff4b5b1d',
    ),
    'pear/pear_exception' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b14fbe2ddb0b9f94f5b24cf08783d599f776fff0',
    ),
    'pear/validate_finance_creditcard' => 
    array (
      'pretty_version' => '0.7.0',
      'version' => '0.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f138fb80b2305c1fe7ca33216b895b868396f1e9',
    ),
    'phenx/php-font-lib' => 
    array (
      'pretty_version' => '0.5.4',
      'version' => '0.5.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dd448ad1ce34c63d09baccd05415e361300c35b4',
    ),
    'phenx/php-svg-lib' => 
    array (
      'pretty_version' => '0.5.0',
      'version' => '0.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '76876c6cf3080bcb6f249d7d59705108166a6685',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fde2ccf55eaef7e86021ff1acce26479160a0fa0',
    ),
    'phpoffice/phpword' => 
    array (
      'pretty_version' => '0.18.3',
      'version' => '0.18.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be0190cd5d8f95b4be08d5853b107aa4e352759a',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '2.0.45',
      'version' => '2.0.45.0',
      'aliases' => 
      array (
      ),
      'reference' => '28d8f438a0064c9de80857e3270d071495544640',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '^1.0',
        1 => '1.0',
      ),
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb5906edc1c324c9a05aa0873d40117941e5fa90',
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e616d01114759c4c489f93b099585439f795fe35',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'v0.11.22',
      'version' => '0.11.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '128fa1b608be651999ed9789c95e6e2a31b5802b',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'roundcube/plugin-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'rsky/pear-core-min' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.10.13',
      ),
    ),
    'rubobaquero/phpquery' => 
    array (
      'pretty_version' => '0.9.15',
      'version' => '0.9.15.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2c3c1bb8a9d48dd0f1230bda2413ce38108b5fe',
    ),
    'sabberworm/php-css-parser' => 
    array (
      'pretty_version' => '8.4.0',
      'version' => '8.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e41d2140031d533348b2192a83f02d8dd8a71d30',
    ),
    'scssphp/scssphp' => 
    array (
      'pretty_version' => 'v1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ace2503684bab0dcc817d7614c8a54b865122414',
    ),
    'shama/baton' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'stack/builder' => 
    array (
      'pretty_version' => 'v1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a4faaa6f532c6086bc66c29e1bc6c29593e1ca7c',
    ),
    'stecman/symfony-console-completion' => 
    array (
      'pretty_version' => '0.11.0',
      'version' => '0.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9502dab59405e275a9f264536c4e1cb61fc3518',
    ),
    'symfony-cmf/routing' => 
    array (
      'pretty_version' => '2.3.4',
      'version' => '2.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbcdf2f6301d740454ba9ebb8adaefd436c36a6b',
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ed42f8f9da528d2c6cae36fe1f380b0c1d8f0658',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.49',
      'version' => '4.4.49.0',
      'aliases' => 
      array (
      ),
      'reference' => '33fa45ffc81fdcc1ca368d4946da859c8cdb58d9',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a692492190773c5310bc7877cb590c04c2f05be',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => 'v4.4.49',
      'version' => '4.4.49.0',
      'aliases' => 
      array (
      ),
      'reference' => '9065fe97dbd38a897e95ea254eb5ddfe1310f734',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8b495ea28c1d97b5e0c121748d6f9b53d075c66',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be731658121ef2d8be88f3a1ec938148a9237291',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => '1e866e9e5c1b22168e0ce5f0b467f19bba61266a',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v1.1.13',
      'version' => '1.1.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d5cd762abaa6b2a4169d3e77610193a7157129e',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v4.4.42',
      'version' => '4.4.42.0',
      'aliases' => 
      array (
      ),
      'reference' => '815412ee8971209bd4c1eecd5f4f481eacd44bf5',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => '66bd787edb5e42ff59d3523f623895af05043e4f',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba6a9f0e8f3edd190520ee3b9a958596b6ca2e70',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v4.4.49',
      'version' => '4.4.49.0',
      'aliases' => 
      array (
      ),
      'reference' => '191413c7b832c015bb38eae963f2e57498c3c173',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v4.4.50',
      'version' => '4.4.50.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aa6df6c045f034aa13ac752fc234bb300b9488ef',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v5.4.13',
      'version' => '5.4.13.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb2ccf759e2b967dcd11bdee5bdf30dddd2290bd',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '927013f3aac555983a5059aada98e1907d842695',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '875e90aeea2777b6f135677f618529449334a612',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '639084e360537a19f9ee352433b84ce831f3d2da',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '19bd1e4fcd5b91116f14d8533c57831ed00571b6',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '70f4aebd92afca2f865444d30a4d2151c13c3179',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe2f306d1d9d346a7fee353d0d5012e401e984b5',
    ),
    'symfony/polyfill-php74' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b755b41a155c89f1af29cc33305538499fa05ea',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
    ),
    'symfony/polyfill-php81' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7581cd600fa9fd681b797d00b02f068e2f13263b',
    ),
    'symfony/polyfill-php82' => 
    array (
      'pretty_version' => 'v1.28.0',
      'version' => '1.28.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7716bea9c86776fb3362d6b52fe1fc9471056a49',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cee9cdc4f7805e2699d9fd66991a0e6df8252a2',
    ),
    'symfony/psr-http-message-bridge' => 
    array (
      'pretty_version' => 'v2.1.4',
      'version' => '2.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a125b93ef378c492e274f217874906fb9babdebb',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f7751fd8b60a07f3f349947a309b5bdfce22d6ae',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => 'v4.4.47',
      'version' => '4.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e01d63c55657930a6de03d6e36aae50af98888d',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b426aac47d6427cc1a1d0f7e2ac724627f5966c',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.4.29',
      'version' => '5.4.29.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e41bdc93def20eaf3bfc1537c4e0a2b0680a152d',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v4.4.47',
      'version' => '4.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => '45036b1d53accc48fe9bab71ccd86d57eba0dd94',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '136b19dd05cdf0709db6537d058bcab6dd6e2dbe',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/validator' => 
    array (
      'pretty_version' => 'v4.4.48',
      'version' => '4.4.48.0',
      'aliases' => 
      array (
      ),
      'reference' => '54781a4c41efbd283b779110bf8ae7f263737775',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.4.29',
      'version' => '5.4.29.0',
      'aliases' => 
      array (
      ),
      'reference' => '6172e4ae3534d25ee9e07eb487c20be7760fcc65',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v4.4.45',
      'version' => '4.4.45.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aeccc4dc52a9e634f1d1eebeb21eacfdcff1053d',
    ),
    'tecnickcom/tcpdf' => 
    array (
      'pretty_version' => '6.4.4',
      'version' => '6.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '42cd0f9786af7e5db4fcedaa66f717b0d0032320',
    ),
    'togos/gitignore' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '32bc0830e4123f670adcbf5ddda5bef362f4f4d4',
    ),
    'totten/ca-config' => 
    array (
      'pretty_version' => 'v22.11.0',
      'version' => '22.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8a78926b11f00e6a154098aeb2110df53b8a3c67',
    ),
    'totten/lurkerlite' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a20c47142b486415b9117c76aa4c2117ff95b49a',
    ),
    'tplaner/when' => 
    array (
      'pretty_version' => 'v3.1.5',
      'version' => '3.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '8865acc235df8f57c2b229517c57c613b11ce4aa',
    ),
    'tubalmartin/cssmin' => 
    array (
      'pretty_version' => 'v4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3cbf557f4079d83a06f9c3ff9b957c022d7805cf',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v2.15.5',
      'version' => '2.15.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc02a6af3eeb97c4bf5650debc76c2eda85ac22e',
    ),
    'typo3/phar-stream-wrapper' => 
    array (
      'pretty_version' => 'v3.1.7',
      'version' => '3.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cc2f04a4e2f5c7e9cc02a3bdf80fae0f3e11a8c',
    ),
    'webflo/drupal-finder' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c8e5dbe65caef285fec8057a4c718a0d4138d1ee',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '11cb2199493b2f8a3b53e7f19068fc6aac760991',
    ),
    'webmozart/path-util' => 
    array (
      'pretty_version' => '2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd939f7edc24c9a1bb9c0dee5cb05d8e859490725',
    ),
    'xkerman/restricted-unserialize' => 
    array (
      'pretty_version' => '1.1.12',
      'version' => '1.1.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '4c6cadbb176c04d3e19b9bb8b40df12998460489',
    ),
    'zetacomponents/base' => 
    array (
      'pretty_version' => '1.9.4',
      'version' => '1.9.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b6ae5f6177f6e51c5fc3514800e1c3fb076ec4be',
    ),
    'zetacomponents/mail' => 
    array (
      'pretty_version' => '1.9.6',
      'version' => '1.9.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '17b57e916e5fa7844abb48a22b4ff873aef0d148',
    ),
  ),
);
